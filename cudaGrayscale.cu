#include <cuda.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

__global__ void convertToGrayscaleGPU(uchar *datosEntrada,uchar *datosSalida, int width, int height)
{
	int i = floor((float)(blockDim.x*blockIdx.x+threadIdx.x)/(float)width);
	int j = (blockDim.x*blockIdx.x+threadIdx.x)%width;
	int pixelIdxMemory = 3*(i*width+j);
	int pixelIdxMemoryGS = i*width+j;
	uchar b = datosEntrada[pixelIdxMemory];
	uchar g = datosEntrada[pixelIdxMemory+1];
	uchar r = datosEntrada[pixelIdxMemory+2];
	uchar level = round((float)r*0.21+(float)g*0.72+(float)b*0.07);
	datosSalida[pixelIdxMemoryGS] = level;
}
void convertToGrayscaleCPU(uchar *datosEntrada,uchar *datosSalida, int width, int height)
{
	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++)
		{
			int pixelIdxMemory = 3*(i*width+j);
			int pixelIdxMemoryGS = i*width+j;
			uchar b = datosEntrada[pixelIdxMemory];
			uchar g = datosEntrada[pixelIdxMemory+1];
			uchar r = datosEntrada[pixelIdxMemory+2];
			uchar level = round((float)r*0.21+(float)g*0.72+(float)b*0.07);
			datosSalida[pixelIdxMemoryGS] = level;
		}
}

int main(int argc, char **argv)
{
	if(argc!=3)
	{
		printf("[ERROR] Uso del programa: %s imagenEntrada imagenSalida\n",
				argv[0]);
		exit(0);
	}
	Mat imagenEntrada;
	imagenEntrada = imread(argv[1],CV_LOAD_IMAGE_COLOR);
	namedWindow("Imagen de entrada",WINDOW_AUTOSIZE);
	imshow("Imagen de entrada",imagenEntrada);
	uchar *datosSalida;
	int width, height;
	width = imagenEntrada.cols;
	height = imagenEntrada.rows;
	printf("%dx%d\n",width,height);
	datosSalida = (uchar *)malloc(sizeof(uchar)*height*width);
	convertToGrayscaleCPU(imagenEntrada.data,datosSalida,width,height);
	Mat imagenSalida(height,width,CV_8U,datosSalida);
	namedWindow("Imagen de salida CPU",WINDOW_AUTOSIZE);
	imshow("Imagen de salida CPU",imagenSalida);

	//Copia de datos a dispositivo
	uchar *d_datosEntrada, *d_datosSalida;
	cudaMalloc((void **)&d_datosEntrada,sizeof(uchar)*width*height*3);
	cudaMalloc((void **)&d_datosSalida,sizeof(uchar)*width*height);
	cudaMemcpy(d_datosEntrada,imagenEntrada.data,sizeof(uchar)*width*height*3,cudaMemcpyHostToDevice);
	//Ejecución
	dim3 block(32);
	dim3 grid(ceil(width*height/block.x));
	convertToGrayscaleGPU<<<grid,block>>>(d_datosEntrada,d_datosSalida,width,height);
	cudaDeviceSynchronize();
	//Copia de datos a CPU
	cudaMemcpy(datosSalida,d_datosSalida,sizeof(uchar)*width*height,cudaMemcpyDeviceToHost);
	imagenSalida = Mat(height,width,CV_8U,datosSalida);
	namedWindow("Imagen de salida GPU",WINDOW_AUTOSIZE);
	imshow("Imagen de salida GPU",imagenSalida);

	waitKey(0);
	return 0;
}
